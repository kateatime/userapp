
function render(arrUsers){
    const UsersArea = document.getElementById('UsersArea');
    arrUsers.forEach(({id, username}) => {
        const divWrapper = document.createElement('div');
        const div = document.createElement('div');   
        const button = document.createElement('input');

        button.type = `button`
        button.className = 'button'
        divWrapper.className = 'user-wrapper';

        div.id = `id${id}`;
        div.className = 'user';
        div.innerHTML = `${id}. ${username}`;

        UsersArea.appendChild(divWrapper);
        divWrapper.appendChild(div);
        divWrapper.appendChild(button);
    })
    
    const bb = document.querySelectorAll('.button');
    bb.forEach(function(btn, index){
        btn.addEventListener('click', function(){
        GetPosts(index)
    });
});
}
function displayPosts(arrPosts, index){

    const PostsArea = document.getElementById('PostsArea');
    PostsArea.innerHTML = '';
    const arrPostsClick = arrPosts.filter(item => item.userId === index + 1);
    arrPostsClick.forEach(({title, body}) => {
    
        const divWrapper = document.createElement('div');
        const divTitle = document.createElement('div');  
        const divBody = document.createElement('div'); 
        
        divWrapper.className = 'post-wrapper';
        divTitle.className = 'post-title';
        divBody.className = 'post-body';

        divTitle.innerHTML = `${title}`;
        divBody.innerHTML = `${body}`;

        PostsArea.appendChild(divWrapper);
        divWrapper.appendChild(divTitle);
        divWrapper.appendChild(divBody);
    })
}

(function() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(function(response){
        return response.json()
    })
    .then(json => render(json))
})();

function GetPosts(index) {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(function(response){
        return response.json()
    })
    .then(json => displayPosts(json,index))
    
}
